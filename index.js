var rIndex,
table = document.getElementById("table");

// check inputan yang kosong
function checkEmptyInput(){

var isEmpty = false,
    fmeja = document.getElementById("meja").value,
    fmenu = document.getElementById("menu").value,
    fjumlah = document.getElementById("jumlah").value;

    if (fmeja === ""){
        alert("Nomor meja tidak boleh kosong");
        isEmpty = true;
    }
    else if (fmenu === ""){
        alert("Menu pesanan tidak boleh kosong");
        isEmpty = true;
    }
    else if (fjumlah === ""){
        alert("Jumlah pesanan tidak boleh kosong");
        isEmpty = true;
    }
    else if (fjumlah == "0"){
        alert("Jumlah pesanan tidak boleh nol, silahkan edit")
        isEmpty = true;
    }
    return isEmpty;
}

// tambah baris
function addHtmlTableRow(){
// ambil value table dengan ID
// buat baris dan cell baru
// ambil value dari inputan text
// ubah value ke cell table
    if (!checkEmptyInput()){
        var newRow = table.insertRow(table.length),
            cell1 = newRow.insertCell(0),
            cell2 = newRow.insertCell(1),
            cell3 = newRow.insertCell(2),
            fmeja = document.getElementById("meja").value,
            fmenu = document.getElementById("menu").value,
            fjumlah = document.getElementById("jumlah").value;

        cell1.innerHTML = fmeja;
        cell2.innerHTML = fmenu;
        cell3.innerHTML = fjumlah;
    // panggil function untuk mengubah event ke baris baru
    selectedRowToInput();
    }
}

// menampilkan data yang dipilih kedalam tempat input text
function selectedRowToInput(){

    for (var i = 1; i < table.rows.length; i++){
        table.rows[i].onclick = function(){
            // mengambil variable index baris yang dipilih
            rIndex = this.rowIndex;
            document.getElementById("meja").value = this.cells[0].innerHTML;
            document.getElementById("menu").value = this.cells[1].innerHTML;
            document.getElementById("jumlah").value = this.cells[2].innerHTML;
        };
    }
}

selectedRowToInput();

function editHtmlTableSelectedRow(){

    var fmeja = document.getElementById("meja").value,
        fmenu = document.getElementById("menu").value,
        fjumlah = document.getElementById("jumlah").value;
    if (!checkEmptyInput()){
        table.rows[rIndex].cells[0].innerHTML = fmeja;
        table.rows[rIndex].cells[1].innerHTML = fmenu;
        table.rows[rIndex].cells[2].innerHTML = fjumlah;
    }
}

function removeSelectedRow(){

table.deleteRow(rIndex);
// menghapus inputan
document.getElementById("meja").value = "";
document.getElementById("menu").value = "";
document.getElementById("jumlah").value = "";
}